from celery import Celery
app = Celery('tasks',broker="redis://localhost:6379/0")

@app.task(name="check")
def check():
    print("I am checking your stuff")
    return "Hello"
 
@celery.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(10.0, check.s(), name='add every 10')
